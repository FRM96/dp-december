import CodeWorld

main :: IO()
main = drawingOf programa

{--
Problema 1:

  Vamos a definir un conjunto de funciones que nos permitan dibujar un
  tablero de ajedrez de tamaño NxN

  Empezaremos dibujando la fila inferior del tablero. Consideraremos
  que la celda inferior izquierda es el inicio de la fila, que tiene
  coordenadas (0, 0); y color amarillo (ya que el blanco sería difícil
  de ver).

  * Definir colorEn una función que, dado un entero n, nos indica el
    color de la celda n-ésima de la primera fila. Por ejemplo,

colorEn 0 ==> yellow
colorEn 3 ==> black

  * Probar con el siguiente ejemplo (intentar, primero, predecir el efecto).

programa = coloured (colorEn 2) (circle 2)  & coloured (colorEn 3) (circle 3)

  * Definir celdaEn una función que dibuja la n-ésima celda de la
    primera fila

  * Probar como antes pero ahora con:

programa = celdaEn 0 & celdaEn 1 & celdaEn 2 & coordinatePlane

  * Definir fila0 una función que, dado un entero n, dibuja la primera
    fila de un tablero nxn. Dibujar la primera fila de un tablero de
    ajedrez.
--}

{--
Problema 2:

  Definir una función que, dado un número natural n, dibuje n círculos
  concéntricos.
--}

{--
Problema 3:

  Definir una función que, dada una lista de colores, dibuje arcos
  concéntricos, uno con cada uno de los colores de la lista. Usar la
  función para dibujar un arcoiris.
--}

coloresArcoIris :: [Color]
coloresArcoIris = [violet, azure, blue, green, yellow, orange, red] 

