-- Funciones de orden superior y definiciones por plegados.
-- Departamento de Ciencias de la Computación e I.A.
-- Universidad de Sevilla
-- =====================================================================
 
-- ---------------------------------------------------------------------
-- Ejercicio 1. Redefinir por recursión la función
--    takeWhile :: (a -> Bool) -> [a] -> [a]
-- tal que (takeWhile p xs) es la lista de los elemento de xs hasta el
-- primero que no cumple la propiedad p. Por ejemplo,
--    takeWhile' (<7) [2,3,9,4,5]  ==  [2,3]
-- ---------------------------------------------------------------------
 
takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' fx (x:xs) 
    | fx x == True = [x] ++ takeWhile' fx xs
    | otherwise = []
 
-- ---------------------------------------------------------------------
-- Ejercicio 2. Redefinir por recursión la función
--    dropWhile :: (a -> Bool) -> [a] -> [a]
-- tal que (dropWhile p xs) es la lista de eliminando los elemento de xs
-- hasta el primero que no cumple la propiedad p. Por ejemplo,
--    dropWhile' (<7) [2,3,9,4,5]  ==  [9,4,5]
-- ---------------------------------------------------------------------
 
dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' fx (x:xs) 
    | fx x == False = (x:xs)
    | otherwise = dropWhile' fx xs


 
-- ---------------------------------------------------------------------
-- Ejercicio 3. Redefinir, usando foldr, la función concat. Por ejemplo, 
--    concat' [[1,3],[2,4,6],[1,9]]  ==  [1,3,2,4,6,1,9]
-- ---------------------------------------------------------------------
 
concat' :: [[a]] -> [a]
concat' xss = foldr (++) [] xss


-- ---------------------------------------------------------------------
-- Ejercicio 4. La función 
--    divideMedia :: [Double] -> ([Double],[Double])
-- dada una lista numérica, xs, calcula el par (ys,zs), donde ys 
-- contiene los elementos de xs estrictamente menores que la media, 
-- mientras que zs contiene los elementos de xs estrictamente mayores 
-- que la media. Por ejemplo, 
--    divideMedia [6,7,2,8,6,3,4] ==  ([2.0,3.0,4.0],[6.0,7.0,8.0,6.0])
--    divideMedia [1,2,3]         ==  ([1.0],[3.0])
-- Definir la función divideMedia por filtrado y por recursión. 
-- ---------------------------------------------------------------------
 
-- La definición por filtrado es
--divideMediaF :: [Double] -> ([Double],[Double])
media xs = sum xs / fromIntegral(length xs)

divideMediaF xs =(filter (<= media xs) xs, filter (> media xs) xs)
 
-- La definición por recursión es
divideMediaR :: [Double] -> ([Double],[Double])
divideMediaR xs = divMedAux [] []  xs (media xs)

divMedAux a b [] _ = (reverse a,reverse b)
divMedAux a b (x:xs) m | x <= m = divMedAux (x:a) b xs m
                    | otherwise = divMedAux a (x:b) xs m 


-- ---------------------------------------------------------------------
-- Ejercicio 5. Definir la función
--    agrupa :: Eq a => [[a]] -> [[a]]
-- tal que (agrupa xss) es la lista de las listas obtenidas agrupando
-- los primeros elementos, los segundos, ... Por
-- ejemplo, 
--    agrupa [[1..6],[7..9],[10..20]]  ==  [[1,7,10],[2,8,11],[3,9,12]]
--    agrupa []                        ==  []
-- ---------------------------------------------------------------------
 
agrupa :: Eq a => [[a]] -> [[a]]
agrupa [] = []
agrupa xss
    | [] `elem` xss = []
    | otherwise = (map head xss) : agrupa (map tail xss)
-- ---------------------------------------------------------------------
-- Ejercicio 6. Se considera la función 
--    filtraAplica :: (a -> b) -> (a -> Bool) -> [a] -> [b]
-- tal que (filtraAplica f p xs) es la lista obtenida aplicándole a los
-- elementos de xs que cumplen el predicado p la función f. Por ejemplo,
--    filtraAplica (4+) (<3) [1..7]  =>  [5,6]
-- Se pide, definir la función
-- 1. por comprensión,
-- 2. usando map y filter,
-- 3. por recursión y
-- 4. por plegado (con foldr).
-- ---------------------------------------------------------------------
 
-- La definición con lista de comprensión es
filtraAplica_1 :: (a -> b) -> (a -> Bool) -> [a] -> [b]
filtraAplica_1 f p xs = [f x | x <- xs, p x] 

-- La definición con map y filter es
filtraAplica_2 :: (a -> b) -> (a -> Bool) -> [a] -> [b]
filtraAplica_2 f p xs = map f (filter p xs)

-- La definición por recursión es
filtraAplica_3 :: (a -> b) -> (a -> Bool) -> [a] -> [b]
filtraAplica_3 f p [] = []

filtraAplica_3 f p (x:xs) 
    | p x = (f x):filtraAplica_3 f p xs
    | otherwise = filtraAplica_3 f p xs
 
-- La definición por plegado es
filtraAplica_4 :: (a -> b) -> (a -> Bool) -> [a] -> [b]
filtraAplica_4 f p xs = foldr (\acc x -> if p acc then (f acc : x) else x) [] xs
 
-- ---------------------------------------------------------------------
-- Ejercicio 7. Definir, usando recursión, plegado, y acumulador, la 
-- función
--    inversa :: [a] -> [a]
-- tal que (inversa xs) es la inversa de la lista xs. Por ejemplo,
--    inversa [3,5,2,4,7]  ==  [7,4,2,5,3]
-- ---------------------------------------------------------------------

inversaR :: [a] -> [a]
inversaR [] = []
inversaR (x:xs) = (inversaR xs) ++ [x]

inversaP :: [a] -> [a]
inversaP = foldr (\acc x -> x ++ [acc]) [] 

inversaAC :: [a] -> [a]
inversaAC xs = aux [] xs  
    where aux acum [] = []
          aux acum (x:xs) = aux (x:acum) xs 

inversaPI :: [a] -> [a]
inversaPI = foldl (\acc x -> [x] ++ acc) []

-- ---------------------------------------------------------------------
-- Ejercicio 8. La función de plegado foldl está definida por
--    foldl :: (a -> b -> a) -> a -> [b] -> a
--    foldl f ys xs = aux ys xs
--        where aux ys []     = ys
--              aux ys (x:xs) = aux (f ys x) xs
-- Definir, mediante plegado con foldl, la función
--    inversaP' :: [a] -> [a]
-- tal que (inversaP' xs) es la inversa de la lista xs. Por ejemplo,
--    inversaP' [3,5,2,4,7]  ==  [7,4,2,5,3]
-- ---------------------------------------------------------------------

inversaP' :: [a] -> [a]
inversaP' = foldr (\acc x -> x ++ [acc]) [] 

-- ---------------------------------------------------------------------
-- Ejercicio 9. Redefinir, por recursión y plegado la función map. 
-- ---------------------------------------------------------------------

mapR :: (a -> b) -> [a] -> [b]
mapR f [] = []
mapR f (x:xs) = f x : mapR f xs

mapP :: (a -> b) -> [a] -> [b]
mapP f = foldr (\acc x -> f acc : x) [] 

-- ---------------------------------------------------------------------
-- Ejercicio 10. Redefinir, usando foldl y foldr la función filter. Por
-- ejemplo, 
--    filter (<4) [1,7,3,2]  =>  [1,3,2]
-- ---------------------------------------------------------------------

filterL :: (a -> Bool) -> [a] -> [a]
filterL = undefined

filterR :: (a -> Bool) -> [a] -> [a]
filterR p = foldr (\acc x -> if p acc then acc:x else x) []

-- ---------------------------------------------------------------------
-- Ejercicio 11. Definir, mediante recursión, plegado, acumulador, y 
-- plegado con foldl la función
--    sumll :: Num a => [[a]] -> a
-- tal que (sumll xss) es la suma de las sumas de las listas de xss. 
-- Por ejemplo, 
--    sumll [[1,3],[2,5]]  ==  11
-- ---------------------------------------------------------------------

sumllR :: Num a => [[a]] -> a
sumllR [] = 0
sumllR (x:xs) = sum x + sumllR xs

sumllP :: Num a => [[a]] -> a
sumllP = foldr (\acc x -> sum acc + x) 0
sumllA :: Num a => [[a]] -> a
sumllA xss = aux 0 xss
    where aux acc [] = acc
          aux acc (xs:xss) = aux (f acc xs) xss
          f acc xs = sum xs + acc

sumllAP :: Num a => [[a]] -> a
sumllAP = foldl (\acc x -> sum x + acc) 0

-- ---------------------------------------------------------------------
-- Ejercicio 12. Definir, mediante recursión y plegado, la función
--    borra :: Eq a => a -> a -> [a]
-- tal que (borra y xs) es la lista obtenida borrando las ocurrencias de
-- y en xs. Por ejemplo, 
--    borra 5 [2,3,5,6]    ==  [2,3,6]
--    borra 5 [2,3,5,6,5]  ==  [2,3,6]
--    borra 7 [2,3,5,6,5]  ==  [2,3,5,6,5]
-- ---------------------------------------------------------------------

borraR :: Eq a => a -> [a] -> [a]
borraR n [] = []
borraR n (x:xs) 
    | x == n = borraR n xs
    | otherwise = x:borraR n xs

borraP :: Eq a => a -> [a] -> [a]
borraP n = foldr (\acc x -> if acc == n then x else acc:x) []

-- ---------------------------------------------------------------------
-- Ejercicio 13. Definir, mediante recursión y plegado la función
--    diferencia :: Eq a => [a] -> [a] -> [a]
-- tal que (diferencia xs ys) es la diferencia del conjunto xs e ys; es
-- decir el conjunto de los elementos de xs que no pertenecen a ys. Por
-- ejemplo,  
--    diferencia [2,3,5,6] [5,2,7]  ==  [3,6]
-- ---------------------------------------------------------------------

diferenciaR :: Eq a => [a] -> [a] -> [a]
diferenciaR [] [] = []
diferenciaR [] ys = ys
diferenciaR xs [] = xs

diferenciaR (x:xs) (y:ys) = undefined

diferenciaP :: Eq a => [a] -> [a] -> [a]
diferenciaP xs ys = undefined

-- -------------------------------------------------------------------
-- Ejercicio 14. Definir mediante plegado la función 
--    producto :: Num a => [a] -> a
-- tal que (producto xs) es el producto de los elementos de la lista
-- xs. Por ejemplo, 
--    producto [2,1,-3,4,5,-6] == 720
-- ---------------------------------------------------------------------

producto :: Num a => [a] -> a
producto = foldr (*) 1

-- ---------------------------------------------------------------------
-- Ejercicio 15. Definir mediante plegado la función 
--    productoPred :: Num a => (a -> Bool) -> [a] -> a
-- tal que (productoPred p xs) es el producto de los elementos de la
-- lista xs que verifican el predicado p. Por ejemplo, 
--    productoPred even [2,1,-3,4,-5,6] == 48
-- ---------------------------------------------------------------------

productoPred :: Num a => (a -> Bool) -> [a] -> a
productoPred p = foldr (\acc x -> if p acc then acc*x else x) 1
